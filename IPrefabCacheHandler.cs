﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CacheSystem
{
    /// <summary>
    /// Interface for special storage classes, containing logic for work with Prefab Cache component
    /// </summary>
    /// <typeparam name="T">Type of key to be used</typeparam>
    /// <typeparam name="Z">Type of stored MonoBehaviour component</typeparam>
    public interface IPrefabCacheHandler<T, Z> where Z : MonoBehaviour
    {
        Z this[T key] { get; }
        int count { get; }


        /// <summary>
        /// Assign Prefab cache for this class
        /// </summary>
        /// <param name="cache">Prefab cache</param>
        void Init(PrefabCache cache);

        /// <summary>
        /// Clear storage and free all prefabs
        /// </summary>
        void Clear();
    }
}