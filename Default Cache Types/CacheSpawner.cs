using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CacheSystem
{
    /// <summary>
    /// List style prefab cache. Automatically frees switched off prefabs in fixed update
    /// </summary>
    /// <typeparam name="T">Desired class, attached to prefab</typeparam>
    public class CacheSpawner<T> : MonoBehaviour, IPrefabCacheHandler<int, T> where T : MonoBehaviour
    {
        private List<T> activeItems;
        private PrefabCache cache;

        public T this[int key] => activeItems[key];
        public int count => activeItems.Count;

        private bool ready = false;


        public void Init(PrefabCache cache)
        {
            activeItems = new List<T>();
            this.cache = cache;
            ready = true;
        }


        void FixedUpdate()
        {
            if (ready)
                foreach (T item in activeItems.ToArray())
                    if (!item.gameObject.activeSelf)
                    {
                        activeItems.Remove(item);
                        cache.SetFree(item.gameObject.GetInstanceID());
                    }
        }


        public T GetNewItem(string itemName = "")
        {
            var result = cache.GetNew<T>(itemName);

            activeItems.Add(result);
            return result;
        }


        public void Clear()
        {
            cache.Clear();
            activeItems.Clear();
        }
    }
}