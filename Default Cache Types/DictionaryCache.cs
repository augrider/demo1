using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace CacheSystem
{
    /// <summary>
    /// Dictionary style prefab cache (key defined by user). Use this when you don't need logic connected to type of item, only standard storage operations
    /// </summary>
    /// <typeparam name="T">Keys used for getting stored prefab classes</typeparam>
    /// <typeparam name="Z">Class derived from PrefabItem</typeparam>
    public class DictionaryCache<T, Z> : CacheHandler, IPrefabCacheHandler<T, Z> where Z : MonoBehaviour
    {
        private Dictionary<T, Z> activeItems = new Dictionary<T, Z>();

        public Z this[T key] => activeItems[key];
        public Z[] allItems => activeItems.Values.ToArray();
        public int count => activeItems.Count;


        public Z GetNewItem(T key, string itemName = "")
        {
            if (activeItems.ContainsKey(key))
            {
                Debug.LogAssertion("Tried to get new item, but item with the same key exists! Returning recorded");
                return activeItems[key];
            }

            var result = GetNewFromCache<Z>(itemName);

            activeItems.Add(key, result);
            return result;
        }


        public bool TryGetItem(T key, out Z result)
        {
            result = activeItems.ContainsKey(key) ? activeItems[key] : null;

            return result;
        }


        public bool ContainsKey(T key) => activeItems.ContainsKey(key);


        public void Remove(T key)
        {
            if (activeItems.ContainsKey(key))
            {
                SetFree(activeItems[key]);
                activeItems.Remove(key);
            }
        }



        public override IEnumerator GetEnumerator()
        {
            return ((IEnumerable)activeItems).GetEnumerator();
        }

        protected override void ClearStorage() => activeItems.Clear();
    }
}