using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CacheSystem
{
    /// <summary>
    /// Base class for all handlers
    /// </summary>
    public abstract class CacheHandler : IEnumerable
    {
        private PrefabCache cache;


        /// <summary>
        /// Assign Prefab cache for this class
        /// </summary>
        /// <param name="cache">Prefab cache</param>
        public void Init(PrefabCache cache)
        {
            this.cache = cache;
            cache.Clear();
            ClearStorage();

            OnInit(cache);
        }


        protected T GetNewFromCache<T>(string itemName = "") where T : MonoBehaviour
        {
            CheckForCache();
            var result = cache.GetNew<T>(itemName);

            return result;
        }

        protected void SetFree(MonoBehaviour behaviour)
        {
            CheckForCache();
            cache.SetFree(behaviour.gameObject.GetInstanceID());
        }

        /// <summary>
        /// Clear storage and free all prefabs
        /// </summary>
        public void Clear()
        {
            cache.Clear();
            ClearStorage();
        }


        protected virtual void OnInit(PrefabCache cache) { }


        private void CheckForCache()
        {
            if (cache == null)
                throw new NullReferenceException(string.Format("Cache for {0} was not found or set!", this));
        }



        public abstract IEnumerator GetEnumerator();
        protected abstract void ClearStorage();
    }
}