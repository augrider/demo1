﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace CacheSystem
{
    /// <summary>
    /// Cache storage component for prefabs
    /// </summary>
    public sealed class PrefabCache : MonoBehaviour
    {
        [SerializeField] private GameObject prefab;
        [SerializeField] private Transform parent;

        /// <summary>
        /// Amount of copies loaded at the start of the game
        /// </summary>
        [SerializeField] private int preloadAmount = 0;

        /// <summary>
        /// Dictionary of all instantiated prefab items
        /// </summary>
        /// <typeparam name="int">Gameobject Instance ID</typeparam>
        /// <typeparam name="PrefabItem">Prefab Item reference</typeparam>
        private Dictionary<int, PrefabItem> prefabDict = new Dictionary<int, PrefabItem>();


        // Start is called before the first frame update
        void Start()
        {
            Preload(preloadAmount);
        }

        /// <summary>
        /// Load x amount of unused items
        /// </summary>
        /// <param name="amount">Amount of items</param>
        public void Preload(int amount = 1)
        {
            if (amount <= 0)
                return;

            for (int i = 0; i < amount; i++)
                CreateNew(false);

            Clear();
        }


        /// <summary>
        /// Get component of type T from new or available prefab gameObject
        /// </summary>
        /// <param name="itemName">Optional name for object</param>
        /// <typeparam name="T">Type of MonoBehaviour component</typeparam>
        public T GetNew<T>(string itemName = "") where T : MonoBehaviour
        {
            PrefabItem available;

            if (!GetFirstPassive(out available))
                available = CreateNew();

            SetName(available, itemName);
            available.ToggleInUse(true);

            return GetPrefabComponent<T>(available);
        }


        /// <summary>
        /// Check if gameObject is prefab currently in use
        /// </summary>
        /// <param name="index">GameObject Instance ID</param>
        /// <returns>true if prefab with the same ID found and in use</returns>
        public bool ContainsInUse(int index)
        {
            return prefabDict.ContainsKey(index) ? prefabDict[index].inUse : false;
        }


        /// <summary>
        /// Free prefab from use
        /// </summary>
        /// <param name="index">GameObject Instance ID</param>
        public void SetFree(int index)
        {
            if (ContainsInUse(index))
                prefabDict[index].ToggleInUse(false);
        }

        /// <summary>
        /// Free all prefabs from use
        /// </summary>
        public void Clear()
        {
            foreach (PrefabItem clone in prefabDict.Values)
                clone.ToggleInUse(false);
        }



        private PrefabItem CreateNew(bool activate = false)
        {
            var newObj = Instantiate(prefab, parent ? parent : this.transform);

            var objItem = new PrefabItem(newObj);
            prefabDict.Add(objItem.itemID, objItem);

            objItem.ToggleInUse(activate);

            return objItem;
        }


        private void SetName(PrefabItem item, string name = "")
        {
            if (!string.IsNullOrEmpty(name))
                item.prefab.name = name;
        }


        private bool GetFirstPassive(out PrefabItem result)
        {
            result = prefabDict.Values.FirstOrDefault(t => !t.inUse);
            return result != null;
        }

        private T GetPrefabComponent<T>(PrefabItem available) where T : MonoBehaviour
        {
            T result = available.prefab.GetComponent<T>();

            if (result != null)
                return result;

            SetFree(available.itemID);
            Debug.LogErrorFormat("This prefab doesn't contains component of type {0}!", typeof(T));
            return null;
        }
    }
}